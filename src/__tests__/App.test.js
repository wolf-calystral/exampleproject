/* global it */

import App from '../components/App'
import React from 'react'
import * as ReactDOM from 'react-dom'
import { history, store } from '../store'
import { Router, Route, Switch } from 'react-router-dom'
import { Provider } from 'react-redux'

it('renders without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render((
    <Provider store={store}>
      <Router history={history}>
        <Switch>
          <Route path='/' component={App} />
        </Switch>
      </Router>
    </Provider>
  ), div)
})
